#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <string.h>

int cp2(const char *src, const char *dst)
{
    #define assert(a,b) if(!(a)){ ok = false; goto b;}
    #define CACHE 2048

    int fr,to,cc;
    bool ok = true;
    char buf[CACHE];
    struct stat stat;

    assert((fr = open(src, O_RDONLY)) != -1, gtclose);
    assert(fstat(fr, &stat) != -1, closefr);
    assert((to = open(dst, O_WRONLY|O_TRUNC|O_CREAT, stat.st_mode)) != -1, closefr);

    while((cc = read(fr, buf, CACHE*sizeof(char))) > 0)
        assert(write(to, buf, cc) > 0, closeto);

    closeto: assert(close(to) == 0, closefr);
    closefr: assert(close(fr) == 0, gtclose);

    gtclose: return (ok ? EXIT_SUCCESS : EXIT_FAILURE);

    #undef CACHE
    #undef assert
}

int cpmem(const char *src, const char *dst)
{
    #define assert(a,b,c) if(!(a)){ perror(b); ok = false; goto c;}

    int fr,to;
    bool ok = true;
    char dummy = 0;
    struct stat stat;
    void *msrc, *mdst;

    assert((fr = open(src, O_RDONLY)) != -1, src, gtclose);
    assert(      fstat(fr, &stat) == 0, "fstat", closefr);
    assert((to = open(dst, O_RDWR|O_TRUNC|O_CREAT, stat.st_mode)) != -1, dst, closefr);
    assert(      lseek(to, stat.st_size-1, SEEK_SET) != -1, "lseek", closeto);
    assert(      write(to, &dummy, sizeof(char)) > 0, "write", closeto);

    assert((msrc = mmap(NULL, stat.st_size, PROT_READ,  MAP_SHARED, fr, 0)) != NULL, "map src", closeto);
    assert((mdst = mmap(NULL, stat.st_size, PROT_WRITE, MAP_SHARED, to, 0)) != NULL, "map dst", umapsrc);

    memcpy(mdst, msrc, stat.st_size);

    umapdst: assert(munmap(mdst, stat.st_size) == 0, "umap dst", umapsrc);
    umapsrc: assert(munmap(msrc, stat.st_size) == 0, "umap src", closeto);

    closeto: assert(close(to) == 0, "close to", closefr);
    closefr: assert(close(fr) == 0, "close fr", gtclose);

    gtclose: return (ok ? EXIT_SUCCESS : EXIT_FAILURE);

    #undef assert
}

int semaphore(void)
{
    sem_t *mutex = sem_open("iciblabla", O_CREAT|O_RDWR, S_IWUSR|S_IRUSR, 1);

    sleep(30);

    sem_close(mutex);
    sem_unlink("iciblabla");
    return 0;
}

int main(int argc, char const *argv[])
{
    return cpmem(argv[1], argv[2]);
}