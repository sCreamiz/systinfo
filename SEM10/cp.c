#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int cp_mod(char * fname_from, char * fname_to)
{
	int fd1 = open(fname_from ,O_RDONLY);
	if(fd1 < 3)	{
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	struct stat f1stat;

	int fst = fstat(fd1, &f1stat);
	if(fst == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	int fd2 = open(fname_to ,O_RDWR|O_CREAT|O_TRUNC, f1stat.st_mode);
	if(fd2 < 3) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	ssize_t flength = (ssize_t) f1stat.st_size;
	char buffer[flength];

	ssize_t rd1 = read(fd1, (void *)&buffer, flength);
	if(rd1 == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	ssize_t wr2 = write(fd2, (void *)&buffer, flength);
	if(wr2 == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	int cfd1 = close(fd1);
	if(cfd1 == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	int cfd2 = close(fd2);
	if(cfd2 == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		return(-1);
	}

	return(0);
}

int main(int argc, char const *argv[])
{
	if(argc < 3)
		return(EXIT_FAILURE);
	return cp_mod(argv[1],argv[2]);
}