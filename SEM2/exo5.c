#include <stdlib.h>
#include <stdio.h>

int main(int argc, const char *argv[]) {
	
	struct foo_t {
		char a;
		int b;
	}; //__attribute__((packed));
	
	printf("%lu\n", sizeof(struct foo_t));
	
	return EXIT_SUCCESS;
}