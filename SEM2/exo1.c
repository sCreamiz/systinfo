#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main(int argc, const char *argv[]) {
	
	int a;
	long b;
	void * c;
	char * d;
	size_t e;
	uint64_t f;
	
	printf("%lu\n", sizeof(a)); // 4
	printf("%lu\n", sizeof(b)); // 8
	printf("%lu\n", sizeof(c)); // 8
	printf("%lu\n", sizeof(d)); // 8
	printf("%lu\n", sizeof(e)); // 8
	printf("%lu\n", sizeof(f)); // 8

	return EXIT_SUCCESS;
}