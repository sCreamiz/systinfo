#include <stdlib.h>
#include <stdio.h>

typedef
struct Fraction {
	int num;
	int denum;
} fract_t;

typedef
struct Node {
	struct Fraction * data;
	struct Node * next;
} node_t;

/* * * END HEADER * * */

node_t *stack = NULL;

void
push(fract_t *f)
{
	node_t *n;
	n = (node_t *)malloc(sizeof(node_t));
	if (n == NULL)
		exit(EXIT_FAILURE);

	n->data = f;
	n->next = stack;
	stack = n;

	printf("Adding one node\n");
}

fract_t *
pop()
{
	if (stack == NULL)
		return NULL;

	node_t * removed = stack;
	fract_t * f = removed->data;
	stack = stack->next;
	free(removed);
	
	printf("Popping one node\n");
	return f;
}

int
main(int argc, char const *argv[])
{
	fract_t a ={2,2};

	push(&a);
	printf("%d sur %d\n", stack->data->num, stack->data->denum);

	fract_t *n = pop();
	printf("Popé le noeud %d sur %d \n", n->num, n->denum);

	return EXIT_SUCCESS;
}