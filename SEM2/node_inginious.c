#include <stdlib.h>
#include <stdio.h>

typedef struct node {
  int value;
  struct node *next;
} node; 

size_t
lenght(node *list)
{
	int i = 0;
	for(; list != NULL; i++)
		list = list->next;

	return i;
}

size_t
count(node *list, int value)
{
	int i = 0;
	while(list != NULL)
	{
		if(list->value == value)
			i++;
		list = list->next;
	}
	return i;
}

int
push(node **list, int value)
{
	node *n;
	n = (node *)malloc(sizeof(node));
	if (n == NULL)
		return -1;

	n->value = value;
	n->next = *list;
	*list = n;

	return 0;
}

int
pop(node **list)
{
	node *removed = *list;
	int _value = removed->value;

	*list = (*list)->next;
	free(removed);

	return _value;
}

void
free_list(node *list)
{
	while(list != NULL)
	{
		node *removed = list;
		list = list->next;
		free(removed);
	}
}

int
main()
{
	node *head = NULL;

	push(&head, 2);
	printf("%zu\n", lenght(head));
	push(&head, 3);
	push(&head, 4);
	push(&head, 3);
	printf("Done pushing\n");
	printf("%zu\n", lenght(head));
	printf("Waiting count\n");
	printf("%zu\n", count(head, 3));
	printf("%d\n", pop(&head));
	printf("%d\n", pop(&head));
	free_list(head);

	return EXIT_SUCCESS;
}