#include <stdlib.h>
#include <stdio.h>

int pow2(int a, int b) {
	int tmp = b-1, rtrn = a;
	while (tmp > 0) {
		rtrn *= a;
		tmp--;
	}
	return rtrn;
}


int main(int argc, const char *argv[]) {

	int i = 208,count = 0;

	while (!(0x1 & i)) {
		i = i >> 1;
		count++;	
	}

	printf("Le bit le moins significatif est : %d\n", (int) pow2(2,count));

	return EXIT_SUCCESS;
}