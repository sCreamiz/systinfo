#include <stdlib.h>
#include <stdio.h>

int main(int argc, const char *argv[]) {

	char *str = "Test";
	int i;

	printf("Avec les étoiles:\n");
	for(i = 0; *(str+i) != '\0'; i++) {
		printf("\tL'adresse %p contient le caractère %c\n", str+i, *(str+i));	
	}

	printf("\nAvec les crochets:\n");
	for(i = 0; str[i] != '\0'; i++) {
		printf("\tL'adresse %p contient le caractère %c\n", &str[i], str[i]);	
	}

	return EXIT_SUCCESS;
}