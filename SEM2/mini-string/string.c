/*************************************
 * string.c
 *
 * Implémentation des méthodes strlen,
 * strcat et strcasecmp de string.h
 *************************************/

#include <stdlib.h>
#include <stdio.h>
#include "string.h" // <ctype.h> <string.h>

int main(int argc, const char *argv[])
{
    char *len = "Hello World";
    printf("'Hello World' est long de %zu caracteres\n", strlen2(len));

    char cat[80] = "Hello ";
    char *cot = strcat2(cat,"World");
    printf("%s est la concatenation de 'Hello ' + 'World'\n", cot);

    printf("\nCompare '%s' et '%s'\n", len, cot);
    printf("New compare : %d\n", strcasecmp2(len, cot));
    printf("Original compare : %d\n", strcasecmp(len, cot));

    char * gp = "What's up";
    char * pg = "Bro ?";
    printf("\nCompare '%s' et '%s'\n", gp, pg);
    printf("New compare : %d\n", strcasecmp2(gp,pg));
    printf("Original compare : %d\n", strcasecmp(gp,pg));

    return EXIT_SUCCESS;
}

size_t strlen2(const char *s)
{
    size_t i = 0;

    while (s[i] != '\0')
        i++;

    return i;
}

char *strcat2(char *dest, const char *src)
{
    char* start = dest;
    dest += strlen2(dest);

    for (; *src != '\0'; src++, dest++)
        *dest = *src;

    *dest++ = '\0';
    return start;
}

int strcasecmp2(const char *s1, const char *s2)
{
    while ((*s1 != '\0' && *s2 != '\0') && (tolower(*s1) == tolower(*s2)))
    {
        s1++;
        s2++;
    }
    return tolower(*s1) - tolower(*s2);
}