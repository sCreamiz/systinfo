/*************************************
 * string.h
 *
 * Implémentation des méthodes strlen,
 * strcat et strcasecmp de string.h
 *************************************/

#include <ctype.h>
#include <string.h>

size_t strlen2(const char *s);
char *strcat2(char *dest, const char *src);
int strcasecmp2(const char *s1, const char *s2);