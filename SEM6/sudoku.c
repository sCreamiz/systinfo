#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define SUDOKU_SIZE 9
typedef struct {
    int grid[SUDOKU_SIZE][SUDOKU_SIZE];
} sudoku_t;

typedef struct{
    int (*to_find)[SUDOKU_SIZE];
    sudoku_t * sudoku;
} param_t;

void print_grid(int (*grid)[SUDOKU_SIZE])
{
    int i,j;
    for(i = 0; i < SUDOKU_SIZE; i++)
    {
        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}

int is_grid_incorrect(int (*grid)[SUDOKU_SIZE])
{
    int i,j,k,l,found[SUDOKU_SIZE];

    /* Mise à zero */
    for(i = 0; i < SUDOKU_SIZE; i++)
        found[i] = 0;

    /* Check les lignes*/
    for(i = 0; i < SUDOKU_SIZE; i++)
    {
        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            found[ grid[i][j]-1 ]++;
        }

        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            if(found[j] != 1) return 1;
            found[j] = 0;
        }
    }

    /* Check les colonnes */
    for(i = 0; i < SUDOKU_SIZE; i++)
    {
        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            found[ grid[j][i]-1 ]++;
        }

        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            if(found[j] != 1) return 2;
            found[j] = 0;
        }
    }

    /* Check les carrées 3x3 */
    for(i = 0; i < SUDOKU_SIZE; i=i+3)
    {
        for(j = 0; j < SUDOKU_SIZE; j=j+3)
        {
            for(k = i; k < i+3; k++)
            {
                for(l = j; l < j+3; l++)
                {
                    found[ grid[k][l]-1 ]++;
                }
            }

            for(k = 0; k < SUDOKU_SIZE; k++)
            {
                if(found[k] != 1) return 3;
                found[k] = 0;
            }
        }
    }

    return EXIT_SUCCESS;
}

void copy_grid(int (*src)[SUDOKU_SIZE], int (*dest)[SUDOKU_SIZE])
{
    int i,j;
    for(i = 0; i < SUDOKU_SIZE; i++)
        for(j = 0; j < SUDOKU_SIZE; j++)
            dest[i][j] = src[i][j];
}

void * thread_solving(void * param)
{
    int i,j,tempory_grid[SUDOKU_SIZE][SUDOKU_SIZE];
    param_t * _param = (param_t *) param;
    static int has_found = 0;

    copy_grid(_param->sudoku->grid, tempory_grid);

    while(!has_found)
    {
        for(i = 0; i < SUDOKU_SIZE; i++)
        {
            for(j = 0; j < SUDOKU_SIZE; j++)
            {
                if(_param->to_find[i][j] == 1)
                    tempory_grid[i][j] = (rand()%9)+1;
            }
        }

        if(!is_grid_incorrect(tempory_grid))
        {
            has_found = 1;
            copy_grid(tempory_grid, _param->sudoku->grid);
        }
    }

    pthread_exit(NULL);
}

void sudoku_solve(sudoku_t * s, int nthread)
{
    int i,j,err,to_find[SUDOKU_SIZE][SUDOKU_SIZE];
    srand(time(NULL));

    for(i = 0; i < SUDOKU_SIZE; i++)
    {
        for(j = 0; j < SUDOKU_SIZE; j++)
        {
            if( s->grid[i][j] == 0)
                to_find[i][j] = 1;
            else
                to_find[i][j] = 0;
        }
    }

    pthread_t th[nthread];
    param_t param = {to_find , s};

    for(i = 0; i < nthread; i++)
    {
        err = pthread_create( &th[i], NULL, &thread_solving, (void *) &param);
        if(err) printf("Error pthread_create\n");
    }

    for(i = 0; i < nthread; i++)
    {
        err = pthread_join( th[i], NULL);
        if(err) printf("Error pthread_join\n");
    }
}

int main(int argc, char const *argv[])
{
    sudoku_t sudo = {{
        {4,1,7,8,3,6,9,5,2},
        {0,3,0,4,1,9,8,7,6},
        {9,8,6,7,5,2,0,1,4},
        {8,2,4,6,7,5,1,3,9},
        {7,0,3,9,4,1,5,2,8},
        {5,9,1,0,2,8,6,4,7},
        {6,5,2,1,9,4,7,8,3},
        {3,4,0,5,8,7,2,6,1},
        {1,7,8,0,6,3,4,9,5}
    }};

    sudoku_solve(&sudo, 8);

    printf("Solution found\n\n");
    print_grid(sudo.grid);

    return EXIT_SUCCESS;
}