#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <curl/curl.h>

#define ERROR_HTTP 1
#define ERROR_PERFORM 2
#define ERROR_HTML 3
#define ERROR_CURL 4
#define ERROR_USAGE 5
    
typedef struct{
  char *data;
  size_t size;
} Memory;

static int error;

/*
 * Réalloue la taille de la structure Memory
 *
 * Adapté depuis la documentation
 * http://curl.haxx.se/libcurl/c/getinmemory.html
 */
static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    Memory *mem = (Memory *)userp;

    mem->data = realloc(mem->data, mem->size + realsize + 1);
    if(mem->data == NULL)
    {
        /* out of memory! */ 
        fprintf(stderr, "pas assez de mémoire ! (realloc renvoie NULL)\n");
        return 0;
    }

    memcpy(&(mem->data[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->data[mem->size] = 0;

    return realsize;
}

int main(int argc, char const *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "Usage: %s URL_TO_FETCH\n", argv[0]);
        return(ERROR_USAGE);
    }

    CURL *curl;
    CURLcode res;
    const char *url = argv[1];

    Memory chunk;
    chunk.data = malloc(1); /* grandira par le realloc dans WriteMemory */
    chunk.size = 0;         /* pas de donnée au départ */

    /* On init */
    curl_global_init(CURL_GLOBAL_ALL); /* pas thread-safe ! */
    curl = curl_easy_init();

    if(curl != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0); /* (mettre à 1 pour afficher la requete) */
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1); /* pour les erreurs HTTP/1.0 catégorie 4 */
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0"); /* bonne pratique d'avoir un user-agent */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &chunk);
        res = curl_easy_perform(curl);

        if(res == CURLE_HTTP_RETURNED_ERROR)
        {
            /* Si la requête renvoie une erreur HTTP de type 4xx */
            error = ERROR_HTTP;
            fprintf(stderr, "error: HTTP code\n");
            goto destroy;
        }
        else if(res != CURLE_OK)
        {
            /* Si la requête renvoie une erreur générique */
            error = ERROR_PERFORM;
            fprintf(stderr, "error: %s [%d]\n", curl_easy_strerror(res), res);
            goto destroy;
        }
        else if(chunk.data[0] == '<')
        {
            /* Si on tombe sur une page HTML */
            error = ERROR_HTML;
            fprintf(stderr, "error: this is a HTML page\n");
            goto destroy;
        }

        printf("%s\n", chunk.data); /* print ce qui est reçu */
    }
    else
    {
        /* Impossible d'initialiser curl */
        error = ERROR_CURL;
    }

destroy:
    /* On nettoie derrière */
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    free(chunk.data);

    if(error)
        return(error);
    else
        return(EXIT_SUCCESS);
}