#include <stdlib.h>
#include <stdio.h>

typedef struct {
    int numerateur;
    int denominateur;
} fraction_t;

int fraction_compare(const void * a, const void *b)
{
	fraction_t* fa = (fraction_t*) a;
	fraction_t* fb = (fraction_t*) b;

	double valueA = (1.0/fa->denominateur)*fa->numerateur;
	double valueB = (1.0/fb->denominateur)*fb->numerateur;

	if(valueA < valueB)
		return -1;
	else if(valueA > valueB)
		return 1;
	else
		return 0;
}

void fraction_sort(fraction_t * tab, size_t len)
{
	qsort(tab, len, sizeof(fraction_t), fraction_compare);
}

void fraction_sortIgnore3fl(fraction_t * tab, size_t len)
{
	qsort(tab+3, len-6, sizeof(fraction_t), fraction_compare);
}

void print_tab(fraction_t* tab)
{
	for(int i = 0; i < 8; i++)
		printf("%d/%d\n", tab[i].numerateur, tab[i].denominateur);
}

int main(int argc, char const *argv[])
{
	fraction_t fa;fraction_t fb;
	fraction_t fc;fraction_t fd;
	fraction_t fe;fraction_t ff;
	fraction_t fg;fraction_t fh;

	fa.numerateur = 1;
	fa.denominateur = 3;

	fb.numerateur = 1;
	fb.denominateur = 2;

	fc.numerateur = 1;
	fc.denominateur = 5;

	fd.numerateur = 1;
	fd.denominateur = 1;

	fe.numerateur = 1;
	fe.denominateur = 6;

	ff.numerateur = 1;
	ff.denominateur = 9;

	fg.numerateur = 1;
	fg.denominateur = 7;

	fh.numerateur = 1;
	fh.denominateur = 9;

	fraction_t tab[] = { fa, fb, fc, fd, fe, ff, fg, fh};

	print_tab(tab);

	printf("Autre chose\n");

	fraction_sortIgnore3fl(tab,8);

	print_tab(tab);

	return EXIT_SUCCESS;
}