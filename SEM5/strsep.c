#include <stdio.h>
#include <stdlib.h>

char * my_strsep(char ** stringp, const char * delim)
{
	char *s, *token;
	
	if(stringp == NULL)
    	return NULL;

	if(*stringp == NULL)
	    return NULL;

	if(delim == NULL)
	{
	    token = *stringp;
	    *stringp = NULL;
	    return token;
	}

	s = *stringp;
	token = s;

	/* Recherche du délimiteur */
	while(*s != *delim && *s != 0)
		s++;

	/* Check si c'est un délimiteur ou la fin du string */
	if(*s != 0)
	{
		*s = 0;
		*stringp = ++s; /* Update le reste */
	}
	else
	{
		*stringp = NULL;
	}

	return token;
}

int main(int argc, char const *argv[])
{
	char * str = (char *) argv[1];

	char * token = my_strsep( &str, "-");
	printf("%s\n", token);

	token = my_strsep( &str, "-");
	printf("%s\n", token);

	token = my_strsep( &str, "-");
	printf("%s\n", token);

	token = my_strsep( &str, "-");
	printf("%s\n", token);

	return EXIT_SUCCESS;
}