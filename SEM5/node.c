#include <stdlib.h>
#include <stdio.h>

typedef struct node {
    int value; /* valeur du noeud */
    struct node *next; /* pointeur vers l’élément suivant */
} node;

void reverse(node **list)
{
    if(list == NULL)
        return;

    node* new_list = NULL;

    while(*list != NULL)
    {
        node* forward = (*list)->next;
        (*list)->next = new_list;
        new_list = (*list);
        (*list) = forward; 
    }

    *list = new_list;
}

node *append(node *a, node *b)
{
    if(a == NULL)
        return b;

    node* start = a;
    node* previous = NULL;

    while(a != NULL)
    {
        previous = a;
        a = a->next;
    }

    a = previous;
    a->next = b;

    return start;
}

void split(node *list, node **first_half, node **second_half)
{
    *first_half = NULL;
    *second_half = NULL;

    if(list == NULL)
        return;

    size_t len = 0;

    node* temp = list;
    while(temp != NULL)
    {
        len++;
        temp = temp->next;
    }

    *first_half = list;
    temp = NULL;

    int i;
    for(i = 0; i < len/2; i++)
    {
        temp = list;
        list = list->next;
    }

    if(temp != NULL)
    {
        temp->next = NULL;
        *second_half = list;
    }

}

void print_node(node *list) {
    for(; list != NULL; list = list->next)
        printf("%d ", list->value);
    printf("\n");
}

int main()
{
    
    node* n1 = (node*)malloc(sizeof(node));
    node* n2 = (node*)malloc(sizeof(node));
    node* n3 = (node*)malloc(sizeof(node));
    node* n4 = (node*)malloc(sizeof(node));
    node* n5 = (node*)malloc(sizeof(node));
    node* n6 = (node*)malloc(sizeof(node));
    node* n7 = (node*)malloc(sizeof(node));

    n1->value = 1;
    n1->next = NULL;

    n2->value = 2;
    n2->next = n3;

    n3->value = 3;
    n3->next = n4;

    n4->value = 4;
    n4->next = NULL;

    n5->value = 5;
    n5->next = n6;

    n6->value = 6;
    n6->next = n7;

    n7->value = 7;
    n7->next = NULL;

    node* list = n1;  /* Liste 1 */
    node* list2 = n5; /* Liste 2 */

    printf("This is the list\n");
    print_node(list);

    /*
    reverse(&list);
    printf("This reverses the list\n");
    print_node(list);

    printf("This appends\n");
    node* combine = append(list, list2);
    print_node(combine);

    printf("This splits\n");

    */

    node* first;
    node* second;

    split(n1, &first, &second);
    printf("First one:\n");
    print_node(first);
    printf("Second one:\n");
    print_node(second);

    free(n1);free(n2);
    free(n3);free(n4);
    free(n5);free(n6);
    free(n7);

    return EXIT_SUCCESS;
}