#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

void error(int err, char * msg)
{
	fprintf(stderr, "%s a retourné %d, message d'erreur : %s\n", msg, err, strerror(errno));
	exit(EXIT_FAILURE);
}

void * count(void * param)
{
	int * valuePtr = (void *) param;
	int i = 0;

	while(1)
	{
		*valuePtr = i++;
		sleep(1);
	}

	pthread_exit(NULL);
}

int main(int argc, char const *argv[])
{
	int err, i = 0;
	pthread_t first;

	err = pthread_create(&first, NULL, &count, &i);
	if(err)
		error(err, "pthread_create");

	while(1)
	{
		printf("%d ", i); /* CHANGE HERE ! */
		fflush(0);
		sleep(2);
	}

	return EXIT_SUCCESS;
}