/**********************
 * expr.c
 *
 * Permet de calculer
 * des expressions
 **********************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, const char *argv[]) {

	if (argc != 4)
		return EXIT_FAILURE;

	int a = atoi(argv[1]);
	int b = atoi(argv[3]);

	     if (!strcmp(argv[2], "*")) return a*b;
	else if (!strcmp(argv[2], "/")) return a/b;
	else if (!strcmp(argv[2], "+")) return a+b;
	else if (!strcmp(argv[2], "-")) return a-b;
	else if (!strcmp(argv[2], "%")) return a%b;
	else return EXIT_FAILURE;

}