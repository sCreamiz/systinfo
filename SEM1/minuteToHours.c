/**********************
 * minuteToHours.c
 *
 * Transforme les 
 * minutes en heures
 **********************/

#include <stdio.h>
#include <stdlib.h>


void minuteToHours(int* minute, int* hour) {

	// Modification des valeurs pointées par
	// les pointeurs 'hour' et 'minute'.
	*hour = *minute/60;
	*minute = *minute%60;
}

int main(int argc, const char *argv[]) {

	// Création d'integers
	int min = 90;
	int hour = 0;

	// Création de pointeurs vers les integers
	int *minPoint = &min;
	int *hourPoint = &hour;

	// Appelle de méthode avec les pointeurs
	minuteToHours(minPoint,hourPoint);

	printf("Il est exactement %d heure et %d minutes\n", hour, min);
	// Semblable à printf("...", *hourPoint, *minPoint);

	return EXIT_SUCCESS;
}