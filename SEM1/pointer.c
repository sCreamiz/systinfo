/****************************
 * pointer.c
 *
 * Permet de tester l'action
 * des pointeurs
 ****************************/

#include <stdio.h>
#include <stdlib.h>

void addOne(int*);

int main(int argc, const char *argv[])
{
	int nombre = 5;
	int *pointeur = &nombre;

	addOne(pointeur);

	printf("%d",*pointeur);
}

void addOne(int *valeur)
{
	*valeur = *valeur+1;
}