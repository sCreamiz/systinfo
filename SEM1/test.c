/*********************
 * test.c
 *
 * Permet de tester
 * des expressions
 *********************/

#include <stdlib.h>
#include <string.h> //strcmp()

 
int eq (int a, int b) {return !(a==b);}
int ge (int a, int b) {return !(a>=b);}
int gt (int a, int b) {return !(a>b); }
int le (int a, int b) {return !(a<=b);}
int lt (int a, int b) {return !(a<b); }
int ne (int a, int b) {return !(a!=b);}

int main(int argc, const char *argv[])
{ 
    int a = atoi(argv[1]);
    int b = atoi(argv[3]);
    
         if(!strcmp(argv[2],"-eq")) return eq(a,b);
    else if(!strcmp(argv[2],"-ge")) return ge(a,b);
    else if(!strcmp(argv[2],"-gt")) return gt(a,b);
    else if(!strcmp(argv[2],"-le")) return le(a,b);
    else if(!strcmp(argv[2],"-lt")) return lt(a,b);
    else if(!strcmp(argv[2],"-ne")) return ne(a,b);
    else return EXIT_FAILURE;
}