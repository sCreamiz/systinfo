#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <siglongjmp.h>

sigjmp_buf env;
static void sig_handler(int signum) {
    siglongjmp(env,1);
}

unsigned int mysleep(unsigned int seconds)
{
    if(signal(SIGALRM, sig_handler) == SIG_ERR)
        return seconds;
    alarm(seconds);
    if(!sigsetjmp(env,1))
        pause();
    return alarm(0);
}

int main(void)
{
    unsigned int r = mysleep(20);
    printf("%d\n", r);
    return 0;   
}