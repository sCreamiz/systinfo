#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>

int main(void)
{
	int pid, shm_id;

	key_t key = 7337;
	shm_id = shmget(key, getpagesize(), IPC_CREAT | 0644);

	if(shm_id == -1) {
		return 1;
	}

	// Enregistre l'adresse d'attache
	const void *shm_addr = shmat(shm_id, NULL, 0);

	// Ordonne déjà la suppression
	if(shmctl(shm_id, IPC_RMID, 0) == -1) {
		return 1;
	}

	if((pid = fork()) == -1) {
		return 1;
	}

	int i;
	char *tab = (char *)shm_addr;

	if(pid == 0) {
		sprintf(tab, "waifu");
	} else {
		int status;
		wait(&status);
		printf("%s | exit status: %d\n", tab, WEXITSTATUS(status));
	}

	return shmdt(shm_addr);
}