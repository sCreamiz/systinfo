#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int run_command(const char *path, char * const argv[])
{
	int pid, status;
	if((pid = fork()) == -1) return -1;
	if(!pid) {
		//fils
	    return execv(path, argv);
	} else {
		//père
	    if(waitpid(pid, &status, 0) == -1) return -1;
	}   
	return WEXITSTATUS(status);
}

int run_pipe(const char *path_a, char * const argv_a[], const char *path_b, char * const argv_b[])
{
	int fd[2], pid, status;
	if(pipe(fd) == -1) return -1;

	if((pid = fork()) == -1) return -1;
	if(!pid) {
	    //fils 1
	    if(close(fd[0]) == -1) return -1;
	    if(dup2(fd[1], STDOUT_FILENO) == -1) return -1;
	    return execv(path_a, argv_a);
	}

	if(waitpid(pid, &status, 0) == -1) return -1;
	if(WEXITSTATUS(status) != 0) return WEXITSTATUS(status);

	if((pid = fork()) == -1) return -1;
	if(!pid) {
	    //fils 2
	    if(close(fd[1]) == -1) return -1;
	    if(dup2(fd[0], STDIN_FILENO) == -1) return -1;
	    return execv(path_b, argv_b);
	}

	if(close(fd[0]) == -1) return -1;
	if(close(fd[1]) == -1) return -1;

	if(waitpid(pid, &status, 0) == -1) return -1;
	return WEXITSTATUS(status);
}

int run_and(const char *path_a, char * const argv_a[], const char *path_b, char * const argv_b[])
{
	int a;
	if((a = run_command(path_a, argv_a)) == 0) {
		return run_command(path_b, argv_b);
	}
	return a;
}

int run_redirected(const char *path, char * const argv[], const char *output_path)
{
	int fd[2], pid, status, fout;
	if(pipe(fd) == -1) return -1;

	if((pid = fork()) == -1) return -1;
	if(!pid) {
	    //fils
	    if(close(fd[0]) == -1) return -1;
	    if(dup2(fd[1], STDOUT_FILENO) == -1) return -1;
	    return execv(path, argv);
	}

	if(close(fd[1]) == -1) return -1;
	if(waitpid(pid, &status, 0) == -1) return -1;
	if(WEXITSTATUS(status) != 0) return WEXITSTATUS(status);

	char buf;
	if((fout = open(output_path, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH )) == -1) return -1;

	while(read(fd[0], &buf, sizeof(char)) > 0) {
		if(write(fout, &buf, sizeof(char)) == -1) return -1;
	}

	if(close(fout) == -1) return -1;
	if(close(fd[0]) == -1) return -1;

	return WEXITSTATUS(status);
}

int main(void)
{
	const char *p = "/bin/cat";
	char * const a[] = {"./cat", "shell.c", NULL};
	return run_command(p, a);

	// const char *p_a = "/bin/echo";
	// const char *p_b = "/usr/bin/wc";
	// char * const a_a[] = {"./echo", "Happy", "plane", NULL};
	// char * const a_b[] = {"./wc", "-c", NULL};
	// return run_pipe(p_a, a_a, p_b, a_b);

	// const char *p_a = "/bin/echo";
	// const char *p_b = "/bin/echo";
	// char * const a_a[] = {"./echo", "a", NULL};
	// char * const a_b[] = {"./echo", "b", NULL};
	// return run_and(p_a, a_a, p_b, a_b);

	// const char *p = "/bin/echo";
	// char * const a[] = {"./echo", "Hello World", NULL};
	// const char *o = "output.txt";
	// run_redirected(p, a, o);
}